function getRadioChecked(name) {
  document.getElementsByName(name);
  let elemCheked = false;
  let elemValue;
  const elem = document.getElementsByName(name);
  for (var i = 0; i < elem.length; i++) {
    if (elem[i].checked == true) {
      elemCheked = true;
      elemValue = elem[i].value;
    }
  }
  if (elemCheked) {
    return elemValue;
  } else {
    return false;
  }
}

function validateText(text) {
  if (text.value == "") {
    return false;
  } else {
    return true;
  }
}

function validate(campo) {


  var flag = true,
    index;

  const selects = campo
    .closest(".js-v-form-container")
    .getElementsByTagName("select");
  const inputs = campo
    .closest(".js-v-form-container")
    .getElementsByTagName("input");

  // valida solo si existe el radio button de sede
  if (document.querySelector('input[name="sede"]')) {
    if (getRadioChecked("sede")) {
      flag = true;
      document.querySelector(".c-error--sede").style.display = "none";
    } else {
      flag = false;
      document.querySelector(".c-error--sede").style.display = "block";
    }
  }

  for (index = 0; index < inputs.length; index++) {
    var element = inputs[index];

    if (
      element.type === "text" ||
      element.type === "email" ||
      element.type === "number" ||
      element.type === "tel" ||
      element.type === "password"
    ) {
      if (!validateText(element)) {
        element.closest(".c-fieldset").classList.add("c-fieldset__error");
        flag = false;
      } else {
        element.closest(".c-fieldset").classList.remove("c-fieldset__error");
      }

      if (element.classList.contains("js-v-form-rut")) {
        if (getRadioChecked("cedular") === "rut") {
          if (!validateRut(element.value)) {
            element.closest(".c-fieldset").classList.add("c-fieldset__error");
            flag = false;
          } else {
            element
              .closest(".c-fieldset")
              .classList.remove("c-fieldset__error");
          }
        }
      }

      if (element.classList.contains("js-v-form-email")) {
        if (!validateEmail(element.value)) {
          element.closest(".c-fieldset").classList.add("c-fieldset__error");
          flag = false;
        } else {
          element.closest(".c-fieldset").classList.remove("c-fieldset__error");
        }
      }

      if (element.classList.contains("js-v-form-puntaje")) {
        if (!validatePuntaje(element.value)) {
          element.closest(".c-fieldset").classList.add("c-fieldset__error");
          flag = false;
        } else {
          element.closest(".c-fieldset").classList.remove("c-fieldset__error");
        }
      }

      if (element.classList.contains("js-v-form-nota")) {
        if (!validateNota(element.value)) {
          element.closest(".c-fieldset").classList.add("c-fieldset__error");
          flag = false;
        } else {
          element.closest(".c-fieldset").classList.remove("c-fieldset__error");
        }
      }
    }
  }

  for (index = 0; index < selects.length; index++) {
    var element = selects[index];
    if (element.selectedIndex == "0") {
      element.closest(".c-fieldset").classList.add("c-fieldset__error");
      flag = false;
    } else {
      element.closest(".c-fieldset").classList.remove("c-fieldset__error");
    }
  }





  if (flag) {
    return true;
  } else {
    return false;
  }
}

function validateNumber(number) {
  var regex = /^[0-9\-]+$/i;
  return regex.test(number);
}

function validatePhone(phone) {
  
  var regex = /([+56]{3})(\d{9})/;
  var regexPhone = /(\d{9})/;

  if (regex.test(phone) || regexPhone.test(phone)) {
    if (phone.length == 12 || phone.length == 9) {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

function validateNota(nota) {
  nota = nota.replace(/,/g, "");
  if (nota != "" && nota <= 70) {
    return true;
  } else {
    return false;
  }
}

function validatePuntaje(puntaje) {
  if (puntaje != "" && puntaje <= 850 && puntaje >= 450) {
    return true;
  } else {
    return false;
  }
}

function validateEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function formatRut(rut) {
  var value = rut.value.replace(/\./g, "").replace("-", "");

  if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
    value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, "$1.$2.$3-$4");
  } else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, "$1.$2.$3-$4");
  } else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, "$1.$2.$3");
  } else if (value.match(/^(\d)(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{0,2})$/, "$1.$2");
  }
  rut.value = value;
}


function formatPasaporte(pasaporte){


    var p = pasaporte.value;
    p = p.toUpperCase();

    if(p.charAt(0) == "P"){

      pasaporte.value = p;


    }else{

      if(p.length >= 1)
      {
        pasaporte.value = "P"+p;
      }
      
     
    }
    
}

function validateRut(campo) {
  if (campo.length == 0) {
    return false;
  }
  if (campo.length < 8) {
    return false;
  }

  campo = campo.replace("-", "");
  campo = campo.replace(/\./g, "");

  var suma = 0;
  var caracteres = "1234567890kK";
  var contador = 0;
  for (var i = 0; i < campo.length; i++) {
    var u = campo.substring(i, i + 1);
    if (caracteres.indexOf(u) != -1) contador++;
  }
  if (contador == 0) {
    return false;
  }

  var rut = campo.substring(0, campo.length - 1);
  var drut = campo.substring(campo.length - 1);
  var dvr = "0";
  var mul = 2;

  for (i = rut.length - 1; i >= 0; i--) {
    suma = suma + rut.charAt(i) * mul;
    if (mul == 7) mul = 2;
    else mul++;
  }
  var res = suma % 11;
  if (res == 1) dvr = "k";
  else if (res == 0) dvr = "0";
  else {
    var dvi = 11 - res;
    dvr = dvi + "";
  }
  if (dvr != drut.toLowerCase()) {
    return false;
  } else {
    return true;
  }
}


function validatePasaporte(p){

  var regex = /^[P-p]\d+/g;
  return regex.test(p);

}

const buttonValidate = document.getElementsByClassName("js-v-form");

var i;
for (i = 0; i < buttonValidate.length; i++) {

  buttonValidate[i].addEventListener("click", function (e) {
    e.preventDefault();

    if (validate(this)) {

      
      this.closest(".js-v-form-container").submit();
    

      return true;
    } else {
    

      return false;
    }
  });
}




const inputKeyCelular = document.getElementsByClassName("js-v-form-key-celular");

var i;

for (i = 0; i < inputKeyCelular.length; i++) {

  inputKeyCelular[i].addEventListener("keyup", function (evt) {

    var evt = evt ? evt : event;
    var key = evt.charCode ? evt.charCode : evt.keyCode;
      //alert(key);
    return (key <= 13 || (key >= 48 && key <= 57));
  
  });

}

var inputFocusOutCelular = document.getElementsByClassName(
  "js-v-form-key-celular"
);
for (i = 0; i < inputFocusOutCelular.length; i++) {
  inputFocusOutCelular[i].addEventListener("focusout", function () {

    this.value = this.value
      .replace(/\+56/g, "")
      .replace( /(\d{9})/,"+56$1");

  });
}

const inputKeyPuntaje = document.getElementsByClassName("js-v-form-key-puntaje");
var i;
for (i = 0; i < inputKeyPuntaje.length; i++) {
  inputKeyPuntaje[i].addEventListener("keyup", function (e) {
    if (e.which >= 37 && e.which <= 40) return;
    this.value = this.value.replace(/\D/g, "");
  });
}

const inputKeyNota = document.getElementsByClassName("js-v-form-key-nota");
var i;
for (i = 0; i < inputKeyNota.length; i++) {
  inputKeyNota[i].addEventListener("keyup", function (e) {
    if (e.which >= 37 && e.which <= 40) return;
    this.value = this.value
      .replace(/\D/g, "")
      .replace(/\B(?=(\d{1})+(?!\d))/g, ",");
  });
}

var inputKeyRut = document.getElementsByClassName("js-v-form-key-rut");
var i;

for (i = 0; i < inputKeyRut.length; i++) {

  inputKeyRut[i].addEventListener("keyup", function (e) {
 
    if (getRadioChecked("cedular") === "rut") {
      
      e.target.value = rutFormat(e.target.value);
      console.log(formatRut(e.target.value))
    

    }else if(getRadioChecked("cedular") === "pasaporte"){
      formatPasaporte(this);
     
    }
  });


}


var selectCedula = document.querySelectorAll('input[name="cedular"]');
var rut = document.querySelector('#cedula');


for(var i = 0;  i < selectCedula.length; i++){

 
  selectCedula[i].addEventListener('click', function(e){
    

    if(getRadioChecked("cedular") === "rut"){

      rut.setAttribute('placeholder', 'Ejemplo: 12345678-9');
     

    }else if(getRadioChecked("cedular") === "pasaporte"){

      rut.setAttribute('placeholder', 'Ejemplo: P1234567');
     
    }

  });

}


var validateStep4 = document.getElementsByClassName("js-v-form-paso-4");
var inputRange = document.getElementsByClassName('c-points__input-range');

for (i = 0; i < validateStep4.length; i++) {

   
    validateStep4[i].addEventListener('click',function(e){
      
      e.preventDefault();
      var flag = true;

      var inputs = this.closest(".js-v-form-container").getElementsByTagName("input");

      for (var index = 0; index < inputs.length; index++) {
        var element = inputs[index];
    
        if (
          element.type === "tel" ||
          element.type === "number"
        ){

          if (element.classList.contains("js-v-form-nota")) {

                if(!validateNota(element.value)){

                    element.closest(".c-fieldset").classList.add("c-fieldset__error");
                    flag = false;
                }else{
                   
                  element.closest(".c-fieldset").classList.remove("c-fieldset__error");
                  
                }
             
          }

          if (element.classList.contains("js-v-form-points")) {

            if(!validatePuntaje(element.value)){

                element.closest(".c-points").classList.add("c-fieldset__error");
                flag = false;
            }else{
              
              element.closest(".c-points").classList.remove("c-fieldset__error");
              
            }

          }

        }
      }
      
      if(flag){
      
        this.closest(".js-v-form-container").submit();
        return true;
      }else{
      
        return false;
      }
    })

}


// VALIDACIONES Inpus tipo texto

var validateAcronimo = document.querySelectorAll("input.js-v-acronimo,select.js-v-acronimo");

for(var i = 0; i < validateAcronimo.length; i++){

  validateAcronimo[i].addEventListener('blur',function(e){


        // Valida Nombre y Apellido
        if(this.name == 'nombre' || this.name == 'apellido')
        {

          if(!validarTexto(e.target.value)){

              e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
              this.setAttribute('validated','false');
             
              
            
          }else{
      
              e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
              this.setAttribute('validated','true');
             
              
          }

        }
        
        if(this.name == 'cedula'){
          // Valida RUT o Pasaporte
          var tipoCedula = document.querySelector('input[name="cedular"]:checked').value;
        

          if(tipoCedula == 'rut'){

         
            if(!validateRut(e.target.value)){
  
              e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
              this.setAttribute('validated','false');
             
   

            }else{
  
              e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
              this.setAttribute('validated','true');
            
              
            }
  

          }else{
      
            if(!validatePasaporte(e.target.value)){
      
              e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
              this.setAttribute('validated','false');
              
      
            
            }else{
      
              e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
              this.setAttribute('validated','true');
             
              
            }
      
          }


        }
        
        if(this.name == 'email'){
          // Valida campo correo
          if(!validateEmail(e.target.value)){

            e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
            this.setAttribute('validated','false');
           
      
          }else{
      
            e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
            this.setAttribute('validated','true');
           
            
          }

        }

        // Valida celular
        if(this.name == 'celular'){
          
          if(!validatePhone(e.target.value)){

            e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
            this.setAttribute('validated','false');
      
            
      
          }else{
      
            e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
            this.setAttribute('validated','true');
            
          }

        }


         // Valida Nota
        if(this.name == 'nota'){
         
          if(!validateNota(e.target.value)){

            e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
            this.setAttribute('validated','false');
      
            
      
          }else{
      
            e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
            this.setAttribute('validated','true');
            
          }

        }


        // Valida los campos puntajes
        if(this.type == 'tel' && (this.name == 'lectura' || this.name == 'mat' || this.name == 'historia' || this.name == 'ciencia')){
          

    
          if(!validatePuntaje(e.target.value)){

            this.closest(".c-points").classList.add("c-fieldset__error");
            this.setAttribute('validated','false');
            
            
      
          }else{
      
            this.closest(".c-points").classList.remove("c-fieldset__error");
            this.setAttribute('validated','true');
            
          }

        }



        if(this.name == 'demre'){
          

    
          if(!validateText(e.target.value)){

            e.target.closest(".c-fieldset").classList.add("c-fieldset__error");
            this.setAttribute('validated','false');
            
            
      
          }else{
      
            e.target.closest(".c-fieldset").classList.remove("c-fieldset__error");
            this.setAttribute('validated','true');
            
          }

        }

       

        validarAll();
        
   
  });



  validateAcronimo[i].addEventListener('click',function(e){



    if (this.name == 'sede') {
       
      var allInputRadio = this.closest('.p-steps__container-sedes').querySelectorAll("input.js-v-acronimo")
      
      for(i = 0; i < allInputRadio.length; i++) allInputRadio[i].removeAttribute('validated');

      this.setAttribute('validated','true');

    }


    validarAll();

  });




  validateAcronimo[i].addEventListener('change',function(e){

    //this.closest('.p-steps__container-sedes').setAttribute('validated','true');

    if(this.tagName == 'SELECT'){

      this.setAttribute('validated','true');

    }



    /*
    if(this.name == 'integrantes'){
     
      if(e.target.value == 0){
        this.setAttribute('validated','false');
      }else{
        this.setAttribute('validated','true');
      }

    }*/

    validarAll();
   
  });


  validateAcronimo[i].addEventListener('input',function(e){


    if(this.type == 'range' && (this.name == 'lectura' || this.name == 'mat' || this.name == 'historia' || this.name == 'ciencia')){
          

      console.log("Typo Rango")
      if(!validatePuntaje(e.target.value)){

        this.setAttribute('validated','false');
        
      }else{
  
        
        this.setAttribute('validated','true');
        
      }

    }

    validarAll();


  });




}

function validarAll(){

  var input = document.querySelectorAll("input.js-v-acronimo:not(input[type='radio']),select.js-v-acronimo,input[type='radio']:checked.js-v-acronimo");
  var botonSubmit = document.getElementsByClassName('js-v-form-acronimo');
  
  var count = 0;

  for(var i = 0; i < input.length; i++){

      if(input[i].type == 'radio' && input[i].getAttribute('validated') == 'true'){

        count++;
 
      }else if(input[i].getAttribute('validated') == 'true'){

        count++;

      }

  }


 if(count == input.length){

   
    for(var i = 0; i < botonSubmit.length; i++){
      botonSubmit[i].disabled=false;
      botonSubmit[i].classList.remove('c-button__disabled');
    }

 }else{

    for(var i = 0; i < botonSubmit.length; i++){
      botonSubmit[i].disabled=true;
      botonSubmit[i].classList.add('c-button__disabled');
    }

 }

 console.log(input.length+"======"+count)


}

const botonSubmit = document.getElementsByClassName('js-v-form-acronimo');

for(var i = 0; i < botonSubmit.length; i++){

  botonSubmit[i].disabled=true;

}

function validarTexto(campo) {
	if (/^[a-z A-Z ñ Ñ á Á éÉ íÍ óÓ úÚ   ]+$/.test(campo)) {
		return true;
	} else {
		return false;
	}
}



function rutFormat(value, useThousandsSeparator = true) {
  var rutAndDv = splitRutAndDv(value);
  var cRut = rutAndDv[0]; var cDv = rutAndDv[1];
  if(!(cRut && cDv)) { return cRut || value; }
  var rutF = "";
  var thousandsSeparator = useThousandsSeparator ? "." : "";
  while(cRut.length > 3) {
    rutF = thousandsSeparator + cRut.substr(cRut.length - 3) + rutF;
    cRut = cRut.substring(0, cRut.length - 3);
  }
  return cRut + rutF + "-" + cDv;
}



function splitRutAndDv(rut) {
  var cValue = clearFormat(rut);
  if(cValue.length === 0) { return [null, null]; }
  if(cValue.length === 1) { return [cValue, null]; }
  var cDv = cValue.charAt(cValue.length - 1);
  var cRut = cValue.substring(0, cValue.length - 1);
  return [cRut, cDv];
}


function clearFormat(value) {
  return value.replace(/[\.\-\_]/g, "");
}
