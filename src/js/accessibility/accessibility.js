const accessibility = document.getElementsByClassName("js-accessibility");
var i;

if (localStorage.getItem('accesibilidad') == 'true') {
    document.body.classList.add("dark-theme");
}

for (i = 0; i < accessibility.length; i++) {
  accessibility[i].addEventListener("click", function () {
    if (typeof (Storage) !== "undefined") {
      if (localStorage.getItem('accesibilidad') == 'false' || (!localStorage.getItem('accesibilidad'))) {
        localStorage.setItem('accesibilidad', 'true');
        document.body.classList.add("dark-theme");
      } else {
        localStorage.setItem('accesibilidad', 'false');
        document.body.classList.remove("dark-theme");
      }
    }
  });
}