var input_focus = document.getElementsByClassName("c-input-text");
for (i = 0; i < input_focus.length; i++) {
  input_focus[i].addEventListener("focusin", function () {
    this.closest(".c-fieldset").classList.add("c-fieldset__focus");
  });
}

var input_focus = document.getElementsByClassName("c-input-text");
for (i = 0; i < input_focus.length; i++) {
  input_focus[i].addEventListener("focusout", function () {
    this.closest(".c-fieldset").classList.remove("c-fieldset__focus");
  });
}

var select_focus = document.getElementsByClassName("c-select");
for (i = 0; i < select_focus.length; i++) {
  select_focus[i].addEventListener("focusin", function () {
    this.closest(".c-fieldset").classList.add("c-fieldset__focus");
  });
}

var select_focus = document.getElementsByClassName("c-select");
for (i = 0; i < select_focus.length; i++) {
  select_focus[i].addEventListener("focusout", function () {
    this.closest(".c-fieldset").classList.remove("c-fieldset__focus");
  });
}