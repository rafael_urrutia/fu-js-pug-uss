var button_demre = document.getElementsByClassName("js-button-demre");
var i;

if (button_demre) {
  for (i = 0; i < button_demre.length; i++) {
    button_demre[i].addEventListener("click", function (e) {
      e.preventDefault();
      if (this.previousElementSibling.type === "password") {
        this.previousElementSibling.type = "text";
      } else {
        this.previousElementSibling.type = "password";
      }
    });
  }
}