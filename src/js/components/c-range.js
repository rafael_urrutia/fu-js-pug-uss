var inputRange = document.querySelectorAll('input[type="range"]');

for(var i = 0; i < inputRange.length; i++){

    inputRange[i].addEventListener("input",function(e){

            var min = e.target.min,
            max = e.target.max,
            val = e.target.value;

            //console.log(e.target.parentElement.childNodes)
            // Accede al elemento que muestra el puntaje seleccionado
            e.target.parentElement.childNodes[3].innerHTML = '<strong>'+val+'</strong>';
            
            e.target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%';
            e.stopPropagation();    
    })
    // Nueve la barra en el valor inicial
    inputRange[i].style.backgroundSize = (inputRange[i].value - inputRange[i].min) * 100 / (inputRange[i].max - inputRange[i].min) + '% 100%';
    
}
 

changeInputRange();
initRange();



window.onresize = changeInputRange;

// Cambia el type de inputo range a number en desktop
function changeInputRange(){
   
    var listInputRange = document.querySelectorAll('.c-points__input-range');

            if(document.body.scrollWidth < 768){

                for(var i = 0; i < listInputRange.length; i++){
                    
                    listInputRange[i].setAttribute('type', 'range');
                  
                    listInputRange[i].style.backgroundSize = (listInputRange[i].value - listInputRange[i].min) * 100 / (listInputRange[i].max - listInputRange[i].min) + '% 100%';
                }
               

            }else{
                for(var i = 0; i < listInputRange.length; i++){
                listInputRange[i].setAttribute('type', 'tel');
                
                }
               
            } 
}


function initRange(){

    var listInputRange = document.querySelectorAll('.c-points__input-range');

    if(document.body.scrollWidth > 768){

        for(var i = 0; i < listInputRange.length; i++){
            
            listInputRange[i].value= "";
           
        
        }
       

    }else{

        for(var i = 0; i < listInputRange.length; i++){
            
            listInputRange[i].value= 450;
            listInputRange[i].setAttribute('validated','true');
        
        }

    }

}



var input_focus_range = document.getElementsByClassName("c-points__input-range");

for (i = 0; i < input_focus_range.length; i++) {

    input_focus_range[i].addEventListener("focusin", function () {
        this.closest(".c-points__mask").classList.add("c-points__focus");
    });

    input_focus_range[i].addEventListener("focusout", function () {
        this.closest(".c-points__mask").classList.remove("c-points__focus");
      });


}

