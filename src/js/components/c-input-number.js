var input_number = document.getElementsByClassName("c-input-number__button-inc");

for (i = 0; i < input_number.length; i++) {
    input_number[i].addEventListener("click", function (e) {
        e.preventDefault();

        this.parentNode.querySelector('input[type=number]').stepDown();
        //this.parentNode.querySelector('.c-input-number__input span').innerHTML = this.parentNode.querySelector('input[type=number]').value;

        if(this.parentNode.querySelector('input[type=number]').value == 0){
          this.parentNode.querySelector('input[type=number]').setAttribute("validated","false");
        }else{
          this.parentNode.querySelector('input[type=number]').setAttribute("validated","true");
        }
        validarAll();
  });
}

var input_number = document.getElementsByClassName("c-input-number__button-dec");
for (i = 0; i < input_number.length; i++) {
    input_number[i].addEventListener("click", function (e) {

        e.preventDefault();
        this.parentNode.querySelector('input[type=number]').stepUp();
        //this.parentNode.querySelector('.c-input-number__input span').innerHTML = this.parentNode.querySelector('input[type=number]').value;
        if(this.parentNode.querySelector('input[type=number]').value == 0){
          this.parentNode.querySelector('input[type=number]').setAttribute("validated","false");
        }else{
          this.parentNode.querySelector('input[type=number]').setAttribute("validated","true");
        }
        validarAll();
       
  });
}