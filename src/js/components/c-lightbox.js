// function showOverlay() {
//   $('html').addClass('u-overflow');
//   $('.c-overlay').addClass('is-show');
// }

// function hideOverlay() {
//   $('html').removeClass('u-overflow');
//   $('.c-overlay').removeClass('is-show');
// }

var closeModal = document.getElementsByClassName("js-close-modal");
var modal = document.getElementsByClassName("c-lightbox");
var callModal = document.getElementsByClassName("js-call-modal");
var i;

if (closeModal) {
  for (i = 0; i < closeModal.length; i++) {
    closeModal[i].addEventListener("click", function () {
      this.closest(".c-lightbox").classList.remove("is-show");
    });
  }
}

if (callModal) {
  for (i = 0; i < callModal.length; i++) {
    callModal[i].addEventListener("click", function (e) {
      e.preventDefault();
      document
        .getElementById(this.getAttribute("data-modal"))
        .classList.add("is-show");
    });
  }
}
