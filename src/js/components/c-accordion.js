var acc = document.getElementsByClassName("c-accordion__button");
var explanders = document.getElementsByClassName("js-expander");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function () {
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
      this.closest(".c-accordion__list").classList.remove("c-accordion__focus");

      this.setAttribute("aria-expanded", "false");

      // Expander icon
      this.firstElementChild.classList.remove("up");
      this.firstElementChild.classList.add("down");
    } else {
      panel.style.display = "block";
      this.closest(".c-accordion__list").classList.add("c-accordion__focus");

      this.setAttribute("aria-expanded", "true");

      // Expander icon
      this.firstElementChild.classList.remove("down");
      this.firstElementChild.classList.add("up");
    }
  });
}