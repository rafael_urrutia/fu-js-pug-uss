//HTML
import htmlmin from "gulp-htmlmin";

//CSS
import postcss from "gulp-postcss";
import cssnano from "cssnano";
import autoprefixer from "autoprefixer";

//Javascript
import gulp from "gulp";
import babel from "gulp-babel";
import terser from "gulp-terser";

//Pug
import pug from "gulp-pug";

//SASS
import sass from "gulp-sass";

//Common
import concat from "gulp-concat";

//Common
import clean from "gulp-purgecss";

//Cache bust
import cacheBust from "gulp-cache-bust";

//Browser sync
import { init as server, stream, reload } from "browser-sync";

//Plumber
import plumber from 'gulp-plumber';


const production = false;

const cssPlugins = [cssnano(), autoprefixer()];

gulp.task("html-min", () => {
  return gulp
    .src("./src/*.html")
    .pipe(plumber())
    .pipe(
      htmlmin({
        collapseWhitespace: true,
        removeComments: true,
      })
    )
    .pipe(gulp.dest("./public"));
});

gulp.task("styles", () => {
  return gulp
    .src("./src/css/*.css")
    .pipe(plumber())
    .pipe(concat("styles-min.css"))
    .pipe(postcss(cssPlugins))
    .pipe(gulp.dest("./public/css"))
    .pipe(stream());
});

gulp.task("babel", () => {
  return gulp
    .src("./src/js/**/*.js")
    .pipe(plumber())
    .pipe(concat("scripts-min.js"))
    .pipe(babel())
    .pipe(terser())
    .pipe(gulp.dest("./public/js"));
});

gulp.task("views", () => {
  return gulp
    .src("./src/views/**/*.pug")
    .pipe(plumber())
    .pipe(
      pug({
        pretty: production ? false : true,
      })
    )
    .pipe(
      cacheBust({
        type: "timestamp",
      })
    )
    .pipe(gulp.dest("./public"));
});

gulp.task("sass", () => {
  return gulp
    .src("./src/scss/main.scss")
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: "compressed",
      })
    )
    .pipe(gulp.dest("./public/css"))
    .pipe(stream());
});

gulp.task("clean", () => {
  return gulp
    .src("./public/css/main.css")
    .pipe(plumber())
    .pipe(
      clean({
        content: ["./public/*.html"],
      })
    )
    .pipe(gulp.dest("./public/css"));
});

gulp.task("default", () => {
  server({
    server: "./public",
  });
  //gulp.watch('./src/*.html', gulp.series('html-min')).on('change', reload);
  //gulp.watch('./src/css/*.css', gulp.series('styles'))
  gulp.watch("./src/views/**/*.pug",  { interval: 1000, usePolling: true },gulp.series("views")).on("change", reload);
  gulp.watch("./src/scss/**/*.scss", { interval: 1000, usePolling: true }, gulp.series("sass"));
  gulp.watch("./src/js/**/*.js",  { interval: 1000, usePolling: true },gulp.series("babel")).on("change", reload);
});